# 28:12 Studios

28:12 Studios is an independant game development studio based out of Huntington, West Virginia that was founded in early 2017.
It is owned by Alex Clay, who handles most of the programming and gameplay design.
Michael Laxton and Chesney Johnson act as contributors, supplying art and code when requested.
This readme contains the professional history of 28:12 Studios

# 28:12 Builds

This project contains current builds for 28:12's projects.
To run any of these projects, open up their respective folder and run the .exe.

## Our Legacy

Our Legacy is a city builder demo where the player can fly around and build different buildings.
It was built over the course of four months in early to mid 2018 as an independant game/side project.
All Programming, Art, and Design was handled by Alex Clay.

## Falling Embers

Falling Embers is a 2D Roguelike demo where the player can pick up and use multiple weapons, execute combos using these weapons, and input various cheat codes.
It was built over the course of three to four months in early to mid 2017 as an independant game/side project.
Programming was handled by Alex Clay and Michael Laxton.
Art was handled by Alex Clay and Eric Lloyd.

## Project Queen

Project Queen is a cutom chess game demo where the board has special effects. In the primary game mode, the outermost edge of the board falls away every ten turns.
It was built in just over a month in late 2018 as an independant game/side project.
Programming was handled by Alex Clay.
Art is from the Unity Asset Store.

## Project Survive

Project Survive is a VR wave based shooter demo where the player attempts to stave off an endless horde.
It was built in two weeks in early 2018 as a class final.
Programming, Art, and Design was handled by Alex Clay.

# Game Jam Projects

## Zork-A-Like

Zork-A-Like is a text adventure demo in which the player inputs different commands to make events happen.
It was built in two days in early 2018 as part of a Unity game jam (42 hrs).
Programming, Art, and Design was handled by Alex Clay.

## Comic Con - Game Jam

Comic Con - Game Jam is a puzzle game in which the player attempts different actions to reach the front of the line.
It was built in two days in early 2019 as part of a game jam (11 hrs).
Programming and Art was handled by Alex Clay.
Design was handled by Alex Clay, Chesney Johnson, and Michael Laxton.

# 28:12 Studios Convention Experience

## WVGDE (October 2016 @ Mountwest Community & Technical College)

Alex Clay did a presentation on Community Management at the expo.

## WVGDE (October 2017 @ Mountwest Community & Technical College)

Alex Clay, Eric Lloyd, Michael Laxton, Tanner Scites, and Chesney Johnson ran a booth at the expo showcasing Falling Embers.
Alex Clay ran a basic and advanced Unity workshop with A.F.O. Studios.
Alex Clay did a presentation on SOLID design principles and their benefits.

## Vector Conference (April 2018 @ Eastern Kentucky University)

Alex Clay showcased Our Legacy in the student showcase section of the conference.

## WVGDE (October 2018 @ Mountwest Community & Technical College)

Alex Clay, Michael Laxton, Jared Adkins, and Chesney Johnson ran a booth at the expo showcasing Falling Embers, Our Legacy, Project Queen, and Project Survive.
Alex Clay and Chesney Johnson ran basic and advanced Unity workshops.

# Appalachian Game Creators Assosciation

## Studio Involvement

28:12 Studios played a major role in the founding and daily operations of AGCA for it's first year. Alex Clay was responsible for unifying the local developers into the organization, and acted on the board as the event coordinator. The studio was involved in all organization events, including the Video Game XPloratation event and the Summer camp it taught in 2018 (both events were hosted at and by the Clay Center in Charleston, WV).